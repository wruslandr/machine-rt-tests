wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ date
Sun 28 Mar 2021 11:07:06 AM +08

wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ uname -a
Linux hpelitebook8470p-ub2004-rt38 5.4.66-rt38 #1 SMP PREEMPT_RT 
Sat Sep 26 16:51:59 +08 2020 x86_64 x86_64 x86_64 GNU/Linux

wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ lsb_release -a
LSB Version:	core-11.1.0ubuntu2-noarch:security-11.1.0ubuntu2-noarch
Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.2 LTS
Release:	20.04
Codename:	focal
wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ 

===========================================================
(1) Operating Systems - HP-EliteBook-8470p Laptop 
===========================================================
Ubuntu 16.04.7 LTS (16.04) linux-4.4.208-rt191 /dev/sda6
Ubuntu 18.04.5 LTS (18.04) linux-4.19.148-rt64 /dev/sda5
Ubuntu-20.04.2 LTS (20.04) linux-5.4.66-rt38   /dev/sdb1

===========================================================
(2) CPU Processors - HP-EliteBook-8470p Laptop 
===========================================================
wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ cat /proc/cpuinfo
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i5-3380M CPU @ 2.90GHz
stepping	: 9
microcode	: 0x21
cpu MHz		: 1197.256
cache size	: 3072 KB
physical id	: 0
siblings	: 4
core id		: 0
cpu cores	: 2
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm cpuid_fault epb pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase smep erms xsaveopt dtherm ida arat pln pts md_clear flush_l1d
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs itlb_multihit srbds
bogomips	: 5786.58
clflush size	: 64
cache_alignment	: 64
address sizes	: 36 bits physical, 48 bits virtual
power management:
....
....
processor	: 1
....
processor	: 2
....
processor	: 3
....

wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ 

===========================================================
(3) RAM Memory - HP-EliteBook-8470p Laptop 
===========================================================
wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ cat /proc/meminfo
MemTotal:       16241716 kB
MemFree:         9500152 kB
MemAvailable:   14071280 kB
Buffers:         1249896 kB
Cached:          2852596 kB
SwapCached:            0 kB
Active:          3588728 kB
Inactive:        1698852 kB
Active(anon):    1451784 kB
Inactive(anon):    38164 kB
Active(file):    2136944 kB
Inactive(file):  1660688 kB
Unevictable:      260644 kB
Mlocked:               0 kB
SwapTotal:       2097148 kB
SwapFree:        2097148 kB
Dirty:                 0 kB
Writeback:             0 kB
AnonPages:       1445752 kB
Mapped:           443464 kB
Shmem:            304864 kB
KReclaimable:     953980 kB
Slab:            1096916 kB
SReclaimable:     953980 kB
SUnreclaim:       142936 kB
KernelStack:       13904 kB
PageTables:        22368 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    10218004 kB
Committed_AS:    6752200 kB
VmallocTotal:   34359738367 kB
VmallocUsed:       30912 kB
VmallocChunk:          0 kB
Percpu:             7776 kB
HardwareCorrupted:     0 kB
CmaTotal:              0 kB
CmaFree:               0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
Hugetlb:               0 kB
DirectMap4k:      366468 kB
DirectMap2M:    16271360 kB
wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ 

===========================================================
(4) Fixed Disks - HP-EliteBook-8470p Laptop 
===========================================================
wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ sudo fdisk -l
....
==========================
Disk /dev/sda: 931.53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: ST1000LM048-2E71
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0xc2388f86

Device     Boot      Start        End    Sectors   Size Id Type
/dev/sda1  *          2048   39063551   39061504  18.6G 82 Linux swap / Solaris
/dev/sda3         39065598 1953523711 1914458114 912.9G  5 Extended
/dev/sda5       1063065600 1953523711  890458112 424.6G 83 Linux
/dev/sda6         39065600 1063065599 1024000000 488.3G 83 Linux

Partition 3 does not start on physical sector boundary.
Partition table entries are not in disk order.

=========================
Disk /dev/sdb: 931.53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: ST1000LM048-2E71
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0xababbdc7

Device     Boot      Start        End    Sectors   Size Id Type
/dev/sdb1  *          2048 1011927039 1011924992 482.5G 83 Linux
/dev/sdb2       1921511424 1953523711   32012288  15.3G 82 Linux swap / Solaris
....

===========================================================
(X) Smartctl Fixed Disks - HP-EliteBook-8470p Laptop 
===========================================================
wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ which smartctl
/usr/sbin/smartctl

wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ sudo smartctl -a /dev/sda
smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.4.66-rt38] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Seagate Barracuda 2.5 5400
Device Model:     ST1000LM048-2E7172
Serial Number:    ZDE0DA0X
LU WWN Device Id: 5 000c50 0a26c2a1d
Firmware Version: SDM1
User Capacity:    1,000,204,886,016 bytes [1.00 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    5400 rpm
Form Factor:      2.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ACS-3 T13/2161-D revision 3b
SATA Version is:  SATA 3.1, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Sun Mar 28 10:46:22 2021 +08
SMART support is: Available - device has SMART capability.
SMART support is: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    0) seconds.
Offline data collection
capabilities: 			 (0x71) SMART execute Offline immediate.
					No Auto Offline data collection support.
					Suspend Offline collection upon new
					command.
					No Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 ( 163) minutes.
Conveyance self-test routine
recommended polling time: 	 (   2) minutes.
SCT capabilities: 	       (0x3035)	SCT Status supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x000f   084   064   006    Pre-fail  Always       -       227675656
  3 Spin_Up_Time            0x0003   099   099   000    Pre-fail  Always       -       0
  4 Start_Stop_Count        0x0032   100   100   020    Old_age   Always       -       955
  5 Reallocated_Sector_Ct   0x0033   100   100   036    Pre-fail  Always       -       0
  7 Seek_Error_Rate         0x000f   080   060   045    Pre-fail  Always       -       8775704089
  9 Power_On_Hours          0x0032   096   096   000    Old_age   Always       -       3919 (137 23 0)
 10 Spin_Retry_Count        0x0013   100   100   097    Pre-fail  Always       -       0
 12 Power_Cycle_Count       0x0032   100   100   020    Old_age   Always       -       907
184 End-to-End_Error        0x0032   100   100   099    Old_age   Always       -       0
187 Reported_Uncorrect      0x0032   100   100   000    Old_age   Always       -       0
188 Command_Timeout         0x0032   100   099   000    Old_age   Always       -       4295032837
189 High_Fly_Writes         0x003a   100   100   000    Old_age   Always       -       0
190 Airflow_Temperature_Cel 0x0022   058   046   040    Old_age   Always       -       42 (Min/Max 25/43)
191 G-Sense_Error_Rate      0x0032   100   100   000    Old_age   Always       -       35
192 Power-Off_Retract_Count 0x0032   100   100   000    Old_age   Always       -       145
193 Load_Cycle_Count        0x0032   055   055   000    Old_age   Always       -       90010
194 Temperature_Celsius     0x0022   042   054   000    Old_age   Always       -       42 (0 21 0 0 0)
197 Current_Pending_Sector  0x0012   100   100   000    Old_age   Always       -       0
198 Offline_Uncorrectable   0x0010   100   100   000    Old_age   Offline      -       0
199 UDMA_CRC_Error_Count    0x003e   200   200   000    Old_age   Always       -       0
240 Head_Flying_Hours       0x0000   100   253   000    Old_age   Offline      -       3552 (143 41 0)
241 Total_LBAs_Written      0x0000   100   253   000    Old_age   Offline      -       19332510040
242 Total_LBAs_Read         0x0000   100   253   000    Old_age   Offline      -       19293086382
254 Free_Fall_Sensor        0x0032   100   100   000    Old_age   Always       -       0

SMART Error Log Version: 1
No Errors Logged

SMART Self-test log structure revision number 1
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ 

HP-EliteBook-8470p Laptop - HARD DISK /dev/sdb
===========================================================
wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ sudo smartctl -a /dev/sdb
smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.4.66-rt38] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Seagate Barracuda 2.5 5400
Device Model:     ST1000LM048-2E7172
Serial Number:    WDEF945E
LU WWN Device Id: 5 000c50 0aa80f25b
Firmware Version: SDM1
User Capacity:    1,000,204,886,016 bytes [1.00 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    5400 rpm
Form Factor:      2.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ACS-3 T13/2161-D revision 3b
SATA Version is:  SATA 3.1, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Sun Mar 28 10:48:58 2021 +08
SMART support is: Available - device has SMART capability.
SMART support is: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    0) seconds.
Offline data collection
capabilities: 			 (0x71) SMART execute Offline immediate.
					No Auto Offline data collection support.
					Suspend Offline collection upon new
					command.
					No Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 ( 165) minutes.
Conveyance self-test routine
recommended polling time: 	 (   2) minutes.
SCT capabilities: 	       (0x3035)	SCT Status supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x000f   067   064   006    Pre-fail  Always       -       5502952
  3 Spin_Up_Time            0x0003   099   099   000    Pre-fail  Always       -       0
  4 Start_Stop_Count        0x0032   099   099   020    Old_age   Always       -       1243
  5 Reallocated_Sector_Ct   0x0033   100   100   036    Pre-fail  Always       -       0
  7 Seek_Error_Rate         0x000f   078   060   045    Pre-fail  Always       -       63941683
  9 Power_On_Hours          0x0032   100   100   000    Old_age   Always       -       772 (137 78 0)
 10 Spin_Retry_Count        0x0013   100   100   097    Pre-fail  Always       -       0
 12 Power_Cycle_Count       0x0032   100   100   020    Old_age   Always       -       341
184 End-to-End_Error        0x0032   100   100   099    Old_age   Always       -       0
187 Reported_Uncorrect      0x0032   100   100   000    Old_age   Always       -       0
188 Command_Timeout         0x0032   100   100   000    Old_age   Always       -       0
189 High_Fly_Writes         0x003a   100   100   000    Old_age   Always       -       0
190 Airflow_Temperature_Cel 0x0022   060   048   040    Old_age   Always       -       40 (Min/Max 25/40)
191 G-Sense_Error_Rate      0x0032   100   100   000    Old_age   Always       -       10
192 Power-Off_Retract_Count 0x0032   100   100   000    Old_age   Always       -       67
193 Load_Cycle_Count        0x0032   098   098   000    Old_age   Always       -       4414
194 Temperature_Celsius     0x0022   040   052   000    Old_age   Always       -       40 (0 18 0 0 0)
197 Current_Pending_Sector  0x0012   100   100   000    Old_age   Always       -       0
198 Offline_Uncorrectable   0x0010   100   100   000    Old_age   Offline      -       0
199 UDMA_CRC_Error_Count    0x003e   200   200   000    Old_age   Always       -       0
240 Head_Flying_Hours       0x0000   100   253   000    Old_age   Offline      -       755 (147 236 0)
241 Total_LBAs_Written      0x0000   100   253   000    Old_age   Offline      -       6919259780
242 Total_LBAs_Read         0x0000   100   253   000    Old_age   Offline      -       16315851597
254 Free_Fall_Sensor        0x0032   100   100   000    Old_age   Always       -       0

SMART Error Log Version: 1
No Errors Logged

SMART Self-test log structure revision number 1
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed without error       00%       447         -
# 2  Short offline       Completed without error       00%       403         -
# 3  Short offline       Completed without error       00%       382         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

wruslan@hpelitebook8470p-ub2004-rt38:~/gitlab/wruslandr$ 

===========================================================
ALHAMDULILLAH 3 TIMES WRY.
===========================================================

